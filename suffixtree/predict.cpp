/******************************************************************************
* NOTE:
*
*   Although my predict function is not complete, I have thoroughly tested 
* and know that my suffix tree is built correctly.  All of my augment functions
* also work appropriately. 
*
*******************************************************************************/

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <math.h>
#include <ctype.h>
using namespace std;


char *all_text;
int text_len;

struct NODE_T {

   int start_index, end_index, score, num_leaves, depth;
   bool flag;
   NODE_T *best;
   NODE_T *first_child;
   NODE_T *next_sibling;
   NODE_T () {start_index = end_index = score = num_leaves = depth = 0; 
              flag = false; best = first_child = next_sibling = NULL;}
};

NODE_T *suffix_root;

// FUNCTION: comparing suffixes
int compare(NODE_T *suffix, int i) {

   int count = 0;

   while(all_text[suffix ->start_index + count] == all_text[i + count]) {
      count++;
      if(suffix ->start_index + count > suffix ->end_index) break;
   }
   return count;
}

// FUNCITON: insert suffix
NODE_T* insert(NODE_T *root, int i) {

   // base case
   if(root == NULL) {
      NODE_T *temp = new NODE_T();
      temp ->start_index = i;
      temp ->end_index = text_len - 1;
      return temp;
   }

   // case 1: no match
   int x = 0;
   NODE_T *temp2 = root;
   while(temp2 != NULL) {
      x  = compare(temp2, i);
      if(x != 0) break;
      temp2 = temp2 ->next_sibling;   
   }

   if(x == 0) {
      NODE_T * node = new NODE_T();
      node ->start_index = i;
      node ->end_index = text_len - 1;
      node ->next_sibling = root ->next_sibling;
      root ->next_sibling = node;
      return root;
   }

   int y = temp2 ->end_index - temp2 ->start_index + 1;
   // case 2: complete match
   if(x == y ) {
      temp2 ->first_child = insert(temp2 ->first_child, i + x);
   }

   // case 3: partial match
   else if(x < y) {
      temp2 ->end_index = temp2 ->start_index + x - 1;

      NODE_T *temp_child = new NODE_T();
      NODE_T *temp_sibling = new NODE_T();

      temp_child ->start_index = temp2 ->start_index + x;
      temp_child ->end_index = temp2 ->end_index;
      temp_child ->first_child = temp2 ->first_child;
      temp_child ->next_sibling = temp_sibling;
     
      temp_sibling ->start_index = i + x;
      temp_sibling ->end_index = text_len - 1;
      temp_sibling ->first_child = NULL;
      temp_sibling ->next_sibling = NULL; 

      temp2 ->first_child = temp_child;
   }
   return root;
}

void augment_flag(NODE_T *root) {

   
   NODE_T *temp = root ->first_child;
   while(temp != NULL) {
      if(!isalpha(all_text[temp ->start_index])) { 
         root ->flag = true;
      }
      augment_flag(temp);
      temp = temp ->next_sibling;
   }
}

void augment_leaves(NODE_T *root) {

   NODE_T *temp = root ->first_child;
   if(temp == NULL) {
      root ->num_leaves = 1;
      return;
   }

   while(temp != NULL) {
      augment_leaves(temp);
      root ->num_leaves += temp ->num_leaves; 
      temp = temp ->next_sibling;
   }
}

void augment_depth(NODE_T *root) {

   NODE_T *temp = root ->first_child;
   if(root ->depth > 80 || root ->flag == false) {
      root ->score = 0;
   }
   else {
      root ->score = root ->num_leaves * pow(root ->depth, 1.2);
   }    

   while(temp != NULL) {
      temp ->depth = root ->depth + root ->end_index - root ->start_index;
      augment_depth(temp);
      temp = temp ->next_sibling;
   }
}

void best(NODE_T *root) {

   NODE_T *temp_best = root;
   NODE_T *temp = root ->first_child;
 
   if(temp == NULL) {
      root ->best = root;
      return;
   }
 
   while(temp != NULL) {
      best(temp);
      if(temp_best ->score < temp ->best ->score) {
         temp_best = temp ->best;
      }
      temp = temp ->next_sibling;
   }
   root ->best = temp_best;
}

void init(void) {
   /* Read text file */
   FILE *fp = fopen ("text", "r");
   fseek (fp, 0, SEEK_END);
   text_len = ftell(fp);
   all_text = (char *)malloc(text_len + 1);
   fseek (fp, 0, SEEK_SET);
   fread (all_text, text_len, 1, fp);
   fclose (fp);
   all_text[text_len] = 0;

   /* Build suffix tree here... */
   suffix_root = new NODE_T();
   suffix_root ->start_index = suffix_root ->end_index = text_len;

   for(int index = 0; index < text_len - 1; index++) {
      if(all_text[index - 1] == ' ' || all_text[index - 1] == '\n') {
         suffix_root ->first_child = insert(suffix_root ->first_child, index); 
      }
   }
   augment_flag(suffix_root);
   augment_leaves(suffix_root);
   augment_depth(suffix_root);
   best(suffix_root);
   cout<< "type: " <<endl;
}
 
int get_character(void) {
   struct termios oldt, newt;
   int ch;
   tcgetattr (STDIN_FILENO, &oldt);
   newt = oldt;
   newt.c_lflag &= ~(ICANON|ECHO);
   tcsetattr (STDIN_FILENO, TCSANOW, &newt);
   ch = getchar();
   tcsetattr (STDIN_FILENO, TCSANOW, &oldt);
   return ch;
}

int predict(char *s, int *start, int *end) {
   
   int len = strlen(s);
   NODE_T *temp = suffix_root;
   
   if(len == 0) return 0;
   for(int i = 0; i < len; i++) {
      temp = temp ->first_child;
      if(temp == NULL) return 0;
      while(all_text[temp ->start_index != s[i]]) {
         temp = temp ->next_sibling;
         if(temp == NULL) return 0;
      }

      int x = 0;
      NODE_T *temp2 = suffix_root;
      while(temp2 != NULL) {
         x  = compare(temp2, i);
         if(x != 0) break;
         temp2 = temp2 ->next_sibling;
      }

      int y = temp2 ->end_index - temp2 ->start_index + 1;
      
      if(x == 0) return 0;
      if(x < y) return 0;
      
      // case 2: complete match
      if(x == y ) {
         *start = temp2 ->best ->end_index - temp2 ->depth - len;
         *end = temp2 ->best ->end_index; 
         return 1;
      }
   }   
}

void process_keystrokes(void) {
   int ch = 0, start, end, i, j, last_len = 0;
   char s[1024] = "";

   /* Hide cursor */
   printf ("\e[?25l");
   while (ch != '\n') {
      for (i=0; i<last_len+1; i++) printf ("\b");
      printf ("\e[0;37;40m%s\e[0;33;40m", s);
      i = strlen(s);
      if (predict(s, &start, &end))
         while (start <= end) {
	    printf ("%c", all_text[start++]);
	    i++;
         }
      printf ("\e[0;37;40m");
      for (j=0; j<last_len-i; j++) printf (" ");
      for (j=0; j<last_len-i; j++) printf ("\b");
      last_len = i;
      fflush(stdout);
      ch = get_character();
      if ((ch == 8 || ch == 127) && s[0]) s[strlen(s)-1] = 0;
      else if (ch >= ' ') {
         s[strlen(s)+1] = 0;
         s[strlen(s)] = ch;
      } 
   }
   printf ("\e[0;37;40m\e[?25h\n");
}

int main(void) {
   init();
   process_keystrokes();
   return 0;
}

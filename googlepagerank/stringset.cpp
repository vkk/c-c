#include <iostream>
#include <string>
#include <assert.h>
#include "stringset.h"

using namespace std;

// Return a hash for string s in range 0...table_size - 1
int hash(string s, int table_size) {
   unsigned int i, h = 0;
   for(i = 0; i < s.length(); i++)
      h = (h * 2917 + (unsigned int)s[i]) % table_size;
   return h;
}

// Allocate table of pointers to nodes, initialized to NULL
NODE_T **set_table(int size) {
   NODE_T **table = new NODE_T *[size];
   for(int i = 0; i < size; i++)
      table[i] = NULL;
   return table;
}

Stringset::Stringset() {
   size = 4; // initial size of table    
   table = set_table(size);
   num = 0;
}

Stringset::~Stringset() {
   for(int i = 0; i < size; i++) {
      while(table[i] != NULL) {
         NODE_T *temp = table[i];
         table[i] = table[i] ->next;
         delete temp;
      }
   }
   delete[] table;
}

// Return true if key is in the set
int Stringset::find(string key) {
   int h = hash(key, size);
   NODE_T *node = table[h];
   while(node != NULL) {
      if(node ->key == key) return node ->value;
      node = node ->next;
   }
   return -1;
}

// Inserts new key, error if key is already in the set
void Stringset::insert(string key, int value) {
   if(find(key) != -1) remove(key);
   num++;

   if(num == size) {
      NODE_T **prev_table = table;
      size *= 2;
      table = set_table(size);

   // Transfer all elements from prev_table into table
   for(int i = 0; i < size/2; i++) {
      NODE_T *node = prev_table[i];
      while(node != NULL) {
        int h = hash(node ->key, size);
        table[h] = new NODE_T(node ->key, node ->value, table[h]);
        NODE_T *empty = node;
        node = node ->next;
        delete empty;
      }
   }

   // De-allocate prev_table
   delete[] prev_table;
   }

   int h = hash(key, size);
   table[h] = new NODE_T(key, value, table[h]);
}

// Removes a key, error if key isn't in the set 
void Stringset::remove(string key) {
   assert (find(key));
   num--;

   int h = hash(key, size);
   if (table[h] ->key == key) {
       // Delete first node
       NODE_T *empty = table[h];
       table[h] = table[h] ->next;
       delete empty;
   } 
   else {
       // Delete from rest of list
       NODE_T *node = table[h];
       while (node ->next ->key != key) node = node ->next;
       NODE_T *empty = node ->next;
       node ->next = empty ->next;
       delete empty;
   }
}

void Stringset::print(void) {
  for (int i = 0; i < size; i++) {
    NODE_T *node = table[i];
    while(node != NULL) {
      cout << node ->key << " " << node ->value << "\n";
      node = node->next;
    }
  }
}


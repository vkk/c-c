#ifndef STRINGSET_H /* Prevent multiple inclusion... */
#define STRINGSET_H

#include <string>
using namespace std;

struct NODE_T {
   int value;
   string key;
   NODE_T *next;
   NODE_T(string k, int v, NODE_T *n) {key = k; value = v; next = n;}
   NODE_T() {value = 0; key = ""; next = NULL;}
};

class Stringset {

   private:
      NODE_T **table;  // array of pointers to linked lists
      int size;      // size of table, as currently allocated
      int num; // number of pages stored in the table

   public:
      Stringset();
      ~Stringset();
      int find(string key);
      void insert(string key, int value);
      void remove(string key);
      void print(void);
      int get_size(void) { return num; }
};

#endif

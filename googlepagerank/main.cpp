#include <string>
#include <fstream>
#include <iostream>
#include "stringset.h"

using namespace std;

struct PAGE_T{

   string url;
   int num_links;
   int num_words;
   double weight;
   double new_weight;
   NODE_T *words;
   NODE_T *links; 
};

struct WORD_T{

   int value;
   string key;
   NODE_T *page;
};

int num_pages;
PAGE_T *pages;
WORD_T *words;
Stringset hash_table, word_hash;

void read_txt(void) {

   int i = 0;
   string s1, s2;
   Stringset string_set;

   ifstream fin;
   fin.open("webpages.txt");
   while(fin >> s1) {
      if(s1 == "NEWPAGE") {
         fin >> s2;
         hash_table.insert(s2, num_pages);
         num_pages++;
      }
   }
   fin.close();

   int index = 0, index2 = 0;
   string http = "http://";
   pages = new PAGE_T[num_pages];

   fin.open("webpages.txt");
   while(fin >> s2) {
      if(s2 == "NEWPAGE") fin >> s2;
      index = hash_table.find(s2);
      pages[index].url = s2;
      pages[index].words = NULL;
      pages[index].links = NULL;
      pages[index].num_links = 0;
      pages[index].num_words = 0;
      while(fin >> s2 && s2 != "NEWPAGE") {
         if(s2.find(http) == string::npos) {  
            pages[index].words = new NODE_T(s2, 1, pages[index].words);
            pages[index].num_words++;
         }
         else {
            if(hash_table.find(s2) != -1) {
               pages[index].links = new NODE_T(s2, hash_table.find(s2), pages[index].links);
               pages[index].num_links++;
            }
         }
      }
   }
   fin.close();
}

void google_pagerank() {
   
   NODE_T *temp;

   for(int j = 0; j < num_pages; j++) pages[j].weight = 0.1 / num_pages;
   for(int k = 0; k < 50; k++) {
      for(int j = 0; j < num_pages; j++) pages[j].new_weight = 0.1 / num_pages;
      for(int i = 0; i < num_pages; i++) {
         temp = pages[i].links;
         while(temp != NULL) {
            int index = temp ->value;
            pages[index].new_weight += 0.9 * pages[i].weight / pages[i].num_links; 
            temp = temp ->next;
         }
      }
      for(int i = 0; i < num_pages; i++) pages[i].weight = pages[i].new_weight;
   }
}

void invert_index() {

   int word_count = 0;
   NODE_T *temp = NULL;
   NODE_T *x = NULL;
   Stringset temp_hash;

   for(int i = 0; i < num_pages; i++) {
      temp = pages[i].words;
      while(temp != NULL) {
         if(word_hash.find(temp ->key) == -1) {
            word_hash.insert(temp ->key, word_count);
            word_count++;
         }
         temp = temp ->next;
      }
   }
   words = new WORD_T[word_count];
   word_count = 0;
   for(int i = 0; i < num_pages; i++) {
      temp = pages[i].words;
      while(temp != NULL) {
         if(temp_hash.find(temp ->key) == -1) {
            temp_hash.insert(temp ->key, word_count);
            words[word_count].key = temp ->key;
            words[word_count].page = NULL; 
            word_count++;
         }
         temp = temp ->next;
      }
   }
   for(int i = 0; i < num_pages; i++) {
      temp = pages[i].words;
      while(temp != NULL) {
         int index = word_hash.find(temp ->key);
         x = new NODE_T;
         x ->key = pages[i].url;
         x ->value = hash_table.find(pages[i].url);
         x ->next = words[index].page;
         words[index].page = x; 
         temp = temp ->next;
      }
   }
}

int main(void) {
  
   cout << "Reading text file." <<"\n";
   read_txt();
   cout << "Text file read." <<"\n";
   cout << "Moving to the google pagerank algorithm and creating inverted index." <<"\n";
   google_pagerank();
   invert_index();
   cout << "Insert search word: ";
  
   string s;
   int index = -1;
  
   while(cin >> s) {
      index = word_hash.find(s);
      if(index == -1) cout << "Not in file.\n";
      else {
         NODE_T *temp = words[index].page;
         while(temp != NULL) {
            cout << (int)(pages[temp ->value].weight * 10000000) << " "
                 << pages[temp ->value].url << "\n";
            temp = temp ->next;
         }
      }
   }
   return 0;
}

/* TO WHOM IT MAY CONCERN:
 *
 *
 * While, my program is not complete, it does do most everything! I tested my k-nearest
 * neighbor function for random points and it works, I just didn't have enough time to
 * cycle through all of the points at the end - - the mean, variance, weighted average, 
 * nearest neighbor, and different distance functions all work as they should! Please
 * test it thoroughly to see! At the end of my program, I simply have it output creating
 * a k-d tree at the second or '1' wine, leaving the '0' out. And then computing the 
 * k-nearest neighbor on the '0' wine.  Printed out are the neighbors and their distance
 * from the query. Thanks!
 */

#include <vector>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <math.h>
#include <stdio.h>
#include <assert.h>

using namespace std;

struct WINE_T {

   double label;
   double attributes[11];
};

struct KdNODE_T {

   WINE_T *wine;
   KdNODE_T *left;
   KdNODE_T *right;
   KdNODE_T (WINE_T *w) { wine = w; left = right = NULL;}
};

int N, D, K, K_count, alpha;
double min_dist = 10000;
KdNODE_T *nearest;
WINE_T **K_nearest;

double mean_var(WINE_T *data) {

   double sum, mean, variance;
   sum = mean = variance = 0;

   for(int i = 0; i < D; i++) {
      sum = mean = variance = 0;  
      for(int j = 0; j < N; j++) {
         sum += data[j].attributes[i];
      }
      mean = sum / N;
      for(int j = 0; j < N; j++) {
         variance += pow((data[j].attributes[i] - mean), 2);
      }
      variance = variance / N;
      variance = sqrt(variance);
      for(int j = 0; j < N; j++) {
         data[j].attributes[i] = (data[j].attributes[i] - mean) / variance;
      }
   }   
}

KdNODE_T *buildTree(WINE_T *data, int depth, int size) {
   
   int root = rand() % size;
   int count_l, count_r;
   count_l = count_r = 0;

   if(size == 1) {
      KdNODE_T *root_t = new KdNODE_T(&data[0]);
      return root_t;
   }

   for(int i = 0; i < size; i++) {
      if(i != root) { 
         if(data[i].attributes[depth % D] <= data[root].attributes[depth % D]) count_l++;
         else count_r++;
      }
   }

   WINE_T *temp_left = new WINE_T [count_l];
   WINE_T *temp_right = new WINE_T [count_r];

   count_l = count_r = 0;
   for(int i = 0; i < size; i++) {
      if(i != root) {
         if(data[i].attributes[depth % D] <= data[root].attributes[depth % D])
            temp_left[count_l++] = data[i];
         else temp_right[count_r++] = data[i];
      } 
   }

   KdNODE_T *root_t = new KdNODE_T(&data[root]);
   
   if(count_l == 0) root_t ->left = NULL; 
   else root_t ->left = buildTree(temp_left, depth + 1, count_l);
      
   if(count_r == 0) root_t ->right = NULL;
   else root_t ->right = buildTree(temp_right, depth + 1, count_r);

   return root_t;
}

double dist(WINE_T *wine1, WINE_T *wine2) {

   double sum = 0;

   for(int j = 0; j < D; j++) {
      sum += pow(wine1 ->attributes[j] - wine2 ->attributes[j], 2);
   }
   return sqrt(sum);
} 

int max_dist(WINE_T *query) {

   int index;
   double max = 0;

   for(int i = 0; i < K; i++) {
      double current = dist(K_nearest[i], query);
      if(current > max) {
         max = current;
         index = i;
      }
   }
   min_dist = dist(K_nearest[index], query);
   return index;
}

void K_dist(KdNODE_T *root, WINE_T *query) {

   double current = dist(root ->wine, query);

   if(K_count < K) {
      K_nearest[K_count] = root ->wine;
      K_count++;
   }

   else {
      if(current < dist(K_nearest[max_dist(query)], query)) {
         K_nearest[max_dist(query)] = root ->wine;
      }
   }
}

void nearest_neighbor(KdNODE_T *root, WINE_T *query, int depth) {

   if(root == NULL) return;

   if(root ->left == NULL && root ->right == NULL) {
      K_dist(root, query);
   }
   if(query ->attributes[depth % D] <= root ->wine ->attributes[depth % D]) {
      nearest_neighbor(root ->left, query, depth + 1);
      K_dist(root, query);
      if(root ->wine ->attributes[depth % D] - query ->attributes[depth % D] < min_dist) {
         nearest_neighbor(root ->right, query, depth + 1);
      }
   } 

   else {
      nearest_neighbor(root ->right, query, depth + 1);
      K_dist(root, query);
      if(query ->attributes[depth % D] - root ->wine ->attributes[depth % D] < min_dist) {
         nearest_neighbor(root ->left, query, depth + 1);
      }
   }
}

double weight(WINE_T *query) {
 
   double sum, weight;
   sum = weight = 0;
   alpha = 100;

   for(int i = 0; i < K; i++) {
      sum += pow(2.7, (1 / alpha) * dist(K_nearest[i], query));
      weight += pow(2.7, (1 / alpha) * dist(K_nearest[i], query)) * K_nearest[i] ->label;
   }
   return weight / sum; 
} 

int main(int argc, char *argv[]) {

   if(argc != 3) {
      cout << "Usage is [wine.txt] [integer 1-10]" << endl;
      exit (1);
   }

   ifstream fin(argv[1]);
   if(fin == NULL) {
      cout << "Input file not opened" << endl;
      exit (1);
   }
   
   K = atoi(argv[2]);
   if(K <1 || K > 10) {
      cout << "k-value entered: " << K << endl;
      cout << "k-value expected: [integer 1-10]" << endl;
      exit (1);
   }
   
   cout << "filed successfully opened." << endl;
   cout << "k-value entered: " << K << endl;

   // N = number of wines; D = number of attributes
   fin >> N >> D;
   
   WINE_T data[N];
   double x, attr, diff, final;
   diff = final = 0;

   K_nearest = new WINE_T*[K];

   // reading wine point and attributes into array
   for(int i = 0; i < N; i++) { 
      fin >> x;
      data[i].label = x;
      for(int j = 0; j < D; j++) {
         fin >> attr;
         data[i].attributes[j] = attr;
      }
   }
   fin.close();

   mean_var(data);
   KdNODE_T *root = buildTree(&data[1], 0, N - 1);
   nearest_neighbor(root, &data[0], 0);
 
   cout << "wine label: " << data[1].label << endl;

   for(int i = 0; i < K; i++) {
      cout << "nearby points: " << K_nearest[i] ->label << ", distance from query: " << dist(K_nearest[i], &data[0]) << endl;
   }

   return 0;
}

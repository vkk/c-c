/********************************************
* Tiffany Verkaik 
* cpp ray tracer, "ray.h"
********************************************/
#ifndef RAY_H
#define RAY_H

#include "vector.h"

typedef struct {

   VECTOR o;
   VECTOR v;

} RAY_T;

class SPHERE {

   private:
      double r;
      VECTOR c;
      
   public:
      SPHERE (double r, double x, double y, double z);
      bool sph_int(RAY_T ray, VECTOR *int_pt, VECTOR *n);
};

class COLOR {

   private:
      double r, g, b;

   public:
      COLOR();
      COLOR (double end_r, double end_g, double end_b);
      COLOR mult(double i);
      COLOR operator*(double i);
      COLOR add(COLOR i);
      COLOR operator+(COLOR i);
      COLOR add(double i);
      COLOR operator+(double i);
      void cap();
      double get_r();
      double get_g();
      double get_b();
};

class LIGHT {

   private:
      VECTOR light_pt;

   public:
      LIGHT (double x, double y, double z);
      COLOR do_light(RAY_T ray, COLOR obj_c, VECTOR int_pt, VECTOR n);      
};

#endif 

/********************************************
* Tiffany Verkaik 
* cpp ray tracer, "color.cpp"
********************************************/
#include <cmath>
#include "ray.h"

COLOR::COLOR() {

   r = 0;
   g = 0;
   b = 0;
}

COLOR::COLOR(double end_r, double end_g, double end_b) {

   r = end_r;
   g = end_g;
   b = end_b;
}

COLOR COLOR::mult(double i) {

   return COLOR (r * i, g * i, b * i);
}

COLOR COLOR::operator* (double i) {

   return mult(i);
}

COLOR COLOR::add(COLOR i) {

   COLOR result;

   result.r = r + i.r;
   result.g = g + i.g;
   result.b = b + i.b;

   return result;
}

COLOR COLOR::operator+ (COLOR i) {

   return add(i);
}

COLOR COLOR::add(double i) {

   return COLOR(r + i, g + i, b + i);
}

COLOR COLOR::operator+ (double i) {

   return add(i);
}


void COLOR::cap() {

   if(r > 1.0) r = 1;
   if(g > 1.0) g = 1;
   if(b > 1.0) b = 1;
}

double COLOR::get_r() {

   return r;
}

double COLOR::get_g() {

   return g;
}

double COLOR::get_b() {

   return b;
}

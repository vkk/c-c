/*******************************************
* Tiffany Verkaik 
* cpp ray tracer, "vector.h"
*******************************************/
#ifndef VECTOR_H
#define VECTOR_H

class VECTOR {

   private:
      double x;
      double y;
      double z;

   public:
      VECTOR ();
      VECTOR(double x, double y, double z);

      double get_x();
      double get_y();
      double get_z();

      double length();
      double dot(VECTOR v);

      VECTOR add(VECTOR v);
      VECTOR operator+ (VECTOR v);

      VECTOR sub(VECTOR v);
      VECTOR operator- (VECTOR v);
     
      VECTOR scaler(double k);
      VECTOR norm();
      
};
#endif

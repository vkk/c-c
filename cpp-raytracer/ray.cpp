/********************************************
* Tiffany Verkaik 
* cpp ray tracer, "ray.cpp"
********************************************/
#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include "vector.h"
#include "ray.h"
using namespace std;

SPHERE::SPHERE(double r, double x, double y, double z) {

   this ->r = r;
   this ->c = VECTOR(x, y, z);
}

bool SPHERE::sph_int(RAY_T ray, VECTOR *int_pt, VECTOR *n) {

   double A, B, C, disc, t0, t1, t;

   ray.v = ray.v.norm();

   A = 1;

   B = 2 * ( (ray.v.get_x() * (ray.o.get_x() - c.get_x())) +
             (ray.v.get_y() * (ray.o.get_y() - c.get_y())) +
             (ray.v.get_z() * (ray.o.get_z() - c.get_z())) );

   C = (pow(ray.o.get_x() - c.get_x(), 2) +
        pow(ray.o.get_y() - c.get_y(), 2) +
        pow(ray.o.get_z() - c.get_z(), 2)) - pow(r, 2);

   disc = pow(B, 2) - 4 * C;
   if(disc <= 0.0) return false;
   t0 = (-B - sqrt(disc)) / 2;
   t1 = (-B + sqrt(disc)) / 2;

   if(t0 <= 0.0 && t1 <= 0.0) return false;
   if(t0 < t1 && t0 > 0.0) t = t0;
   if(t1 < t0 && t1 > 0.0) t = t1;

   *int_pt = (ray.o + ray.v).scaler(t);
   *n = (*int_pt - c).scaler(1 / r);

   return true;
}

LIGHT::LIGHT(double x, double y, double z) {

   light_pt = VECTOR(x, y, z);
}

COLOR LIGHT::do_light(RAY_T ray, COLOR obj_c, VECTOR int_pt, VECTOR n) {

   double cos0, cos1;
   VECTOR light_v, r;
   COLOR result;

   //ambient
   result = obj_c * 0.1;

   light_v = light_pt - int_pt;
   light_v = light_v.norm();
   cos0 = light_v.dot(n);

   //diffuse
   if(cos0 > 0.0) result = result + obj_c * cos0;

   //specular
   if(cos0 > 0.0) {
      r = light_v - n.scaler(2.0 * cos0);
      r = r.norm();
      cos1 = ray.v.dot(r);
      result = result + pow(cos1, 400);
   }

   return result;
}

int main() {

   RAY_T ray;
   SPHERE sph();
   COLOR final_c;
   VECTOR int_pt, n;

   LIGHT light_pt (-10.0, 10.0, 5.0);
   COLOR obj_c (1.0, 0.0, 0.0);
   SPHERE sphere (3, 0, 0, 10);

   printf("P6\n500 500\n255\n");
   for(int y = 0; y < 500.0; y++) {
      for(int x = 0; x < 500.0; x++) {
         ray.o = VECTOR(0.0, 0.0, 0.0);
         ray.v  = VECTOR((-0.5 + x / 500.0), (0.5 - y / 500.0), 1.0);
         ray.v = ray.v.norm();

         if(sphere.sph_int(ray, &int_pt, &n)) {
            obj_c = COLOR(1.0, 0.0, 0.0);
            final_c = light_pt.do_light(ray, obj_c, int_pt, n);
            final_c.cap();
            int r = 255 * final_c.get_r();
            int g = 255 * final_c.get_g();
            int b = 255 * final_c.get_b();

            printf("%c%c%c", (unsigned char)r,
                             (unsigned char)g,
                             (unsigned char)b);
         }

         else {
            int r = 0;
            int g = 0;
            int b = 0;

            printf("%c%c%c", (unsigned char)r,
                             (unsigned char)g,
                             (unsigned char)b);
         }
      }
   }

return 0;
}

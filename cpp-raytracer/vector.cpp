/************************************************
* Tiffany Verkaik 
* cpp ray tracer, "vector.cpp"
************************************************/
#include <cmath>
#include "vector.h"
using namespace std;

VECTOR::VECTOR() {

   this ->x = 0;
   this ->y = 0;
   this ->z = 0;
}

VECTOR::VECTOR(double x, double y, double z) {

   this ->x = x;
   this ->y = y;
   this ->z = z;
}

double VECTOR::get_x() {
 
   return x;
}

double VECTOR::get_y() {

   return y;
}

double VECTOR::get_z() {

   return z;
}

double VECTOR::length() {

   return sqrt(x * x + y * y + z * z);
}

double VECTOR::dot(VECTOR v) {

   return (x * v.x + y * v.y + z * v.z);
}

VECTOR VECTOR::add(VECTOR v) {

   VECTOR result;

   result.x = x + v.x;
   result.y = y + v.y;
   result.z = z + v.z;

   return result;
}

VECTOR VECTOR::operator+(VECTOR v) {

   return add(v);
}

VECTOR VECTOR::sub(VECTOR v) {

   VECTOR result;

   result.x = x - v.x;
   result.y = y - v.y;
   result.z = z - v.z;

   return result;
}

VECTOR VECTOR::operator-(VECTOR v) {

   return sub(v);
} 

VECTOR VECTOR::scaler(double k) {

   VECTOR vk;

   vk.x = k * x;
   vk.y = k * y;
   vk.z = k * z;

   return vk;
}

VECTOR VECTOR::norm() {

   double len;
   VECTOR vn;

   len = length();
   vn.x = x / len;
   vn.y = y / len;
   vn.z = z / len;

   return vn;
}

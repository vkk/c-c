/******************************************
* Tiffany Verkaik 
* ray tracer, "light.h"
******************************************/
#ifndef LIGHT_H
#define LIGHT_H

#include "ray.h"
#include <stdio.h>

COLOR_T c_add(COLOR_T c1, COLOR_T c2);
COLOR_T c_scl(COLOR_T c, double k);
int shadow_test(OBJ_T *close_obj, SCENE_T scene, LIGHT_T light, POINT_T int_pt, POINT_T n, double t);
COLOR_T do_light(SCENE_T scene, OBJ_T *closest_obj, RAY_T ray, POINT_T int_pt, POINT_T n, double t);
COLOR_T set_color(OBJ_T obj, POINT_T int_pt);
COLOR_T checker(OBJ_T obj, POINT_T int_pt);
RAY_T compute_reflect(RAY_T ray, POINT_T n);
COLOR_T get_c(OBJ_T obj, POINT_T int_pt);

#endif

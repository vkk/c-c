/******************************************
* Tiffany Verkaik 
* ray tracer, "sphere.c"
******************************************/
#include "vector.h"
#include "ray.h"
#include <stdio.h>
#include <math.h>

//sphere intersection test
int sphere_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t) {

   double B, C, disc, t0, t1;

   B = 2 * ((ray.v.x * (ray.or.x - objs.type.sphere.c.x)) +
            (ray.v.y * (ray.or.y - objs.type.sphere.c.y)) +
            (ray.v.z * (ray.or.z - objs.type.sphere.c.z)));

   C = pow(ray.or.x - objs.type.sphere.c.x, 2) +
       pow(ray.or.y - objs.type.sphere.c.y, 2) +
       pow(ray.or.z - objs.type.sphere.c.z, 2) -
       pow(objs.type.sphere.r, 2);

   disc = pow(B, 2.0) - (4.0 * C);

   if(disc <= 0.0) return 0;

   t0 = ( (-B) - sqrt(disc) ) / 2.0;
   t1 = ( (-B) + sqrt(disc) ) / 2.0;

   if(t0 <= 0.0 && t1 <= 0.0) return 0;
   if(t0 < t1 && t0 > 0.0) *t = t0;
   if(t1 < t0 && t1 > 0.0) *t = t1;

   int_pt ->x = ray.or.x + (ray.v.x * (*t));
   int_pt ->y = ray.or.y + (ray.v.y * (*t));
   int_pt ->z = ray.or.z + (ray.v.z * (*t));

   n ->x = (int_pt ->x - objs.type.sphere.c.x) / objs.type.sphere.r;
   n ->y = (int_pt ->y - objs.type.sphere.c.y) / objs.type.sphere.r;
   n ->z = (int_pt ->z - objs.type.sphere.c.z) / objs.type.sphere.r;
   
   return 1;
}

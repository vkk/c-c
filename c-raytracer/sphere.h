/******************************************
* Tiffany Verkaik 
* ray tracer, "box.h"
******************************************/
#ifndef SPHERE_H
#define SPHERE_H

#include <stdio.h>

int sphere_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t);

#endif

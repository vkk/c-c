/******************************************
* Tiffany Verkaik 
* ray tracer, "light.c"
******************************************/
#include "light.h"
#include "vector.h"
#include "ray.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

COLOR_T c_add(COLOR_T c1, COLOR_T c2) {

   COLOR_T c3;
   c3.r = c1.r + c2.r;
   c3.g = c1.g + c2.g;
   c3.b = c1.b + c2.b;
   return c3;
}

COLOR_T c_scl(COLOR_T c, double k) {

   COLOR_T ck;
   ck.r = (k * c.r);
   ck.g = (k * c.g);
   ck.b = (k * c.b);
   return ck;
}

int shadow_test(OBJ_T *close_obj, SCENE_T scene, LIGHT_T light, POINT_T int_pt, POINT_T n, double t) {

   RAY_T light_ray;
   OBJ_T *obj;

   light_ray.or = int_pt;
   light_ray.v = v_sub(light.c, int_pt);
   light_ray.v = v_norm(light_ray.v);
   
   for(obj = scene.objs; obj != NULL; obj = obj ->next)
      if(obj != close_obj)
      if(obj ->int_obj(*obj, light_ray, &int_pt, &n, &t) && t > 0) return 1;
   return 0;
}

COLOR_T set_color(OBJ_T obj, POINT_T int_pt) {

   return obj.color;
}

COLOR_T checker(OBJ_T obj, POINT_T int_pt) {

   if( ((int) floor(int_pt.x) +
        (int) floor(int_pt.y) +
        (int) floor(int_pt.z)) & 1) {
        return obj.color;
   }
   else return obj.color2;
}

RAY_T compute_reflect(RAY_T ray, POINT_T n) {

   RAY_T reflect_ray;

   reflect_ray.v = v_sub(ray.v, v_scl(n, 2.0 * (v_dot(n, ray.v))));
   reflect_ray.v = v_norm(reflect_ray.v);

   return reflect_ray;
}

COLOR_T do_light(SCENE_T scene, OBJ_T *closest_obj, RAY_T ray, POINT_T int_pt, POINT_T n, double t) {

    
   double cos0, cos1;
   double c1, c2, c3;
   double light_l, att, reflect_c;
   POINT_T r;
   COLOR_T result;
   LIGHT_T *light;
   RAY_T light_ray;  
 
   COLOR_T obj_color = closest_obj ->get_c(*closest_obj, int_pt);
   reflect_c = closest_obj ->reflect;

   //ambient 
   result.r = obj_color.r * 0.1 * (1 - reflect_c);
   result.g = obj_color.g * 0.1 * (1 - reflect_c);
   result.b = obj_color.b * 0.1 * (1 - reflect_c);

   for(light = scene.lights; light != NULL; light = light ->next) {
      if(shadow_test(closest_obj, scene, *light, int_pt, n, t) == 0) { 
         light_ray.or = int_pt;
         light_ray.v = v_sub(light ->c, int_pt);
         light_ray.v = v_norm(light_ray.v);

         //attenuation computed
         light_l = v_len(light_ray.v);
         c1 = 0.002;
         c2 = 0.02;
         c3 = 0.2;

         att = 1 / ((c1 * pow(light_l, 2)) + (c2 * light_l) + c3); 

         //attenuation capped
         if(att > 1) att = 1;

         //diffuse
         cos0 = v_dot(n, light_ray.v);
   
         if(cos0 > 0.0) {
            result.r += cos0 * obj_color.r * att * (1 - reflect_c);
            result.g += cos0 * obj_color.g * att * (1 - reflect_c);
            result.b += cos0 * obj_color.b * att * (1 - reflect_c);
         } 

         //specular
         r = v_sub(light_ray.v, v_scl(n, (2.0 * cos0)));
         r = v_norm(r);
         cos1 = v_dot(r, ray.v);

         if(cos0 > 0.0 && cos1 > 0.0) {
            result.r += pow(cos1, 400.0) * att;
            result.g += pow(cos1, 400.0) * att;
            result.b += pow(cos1, 400.0) * att;
         }
      }
   }
  
   return result;
}

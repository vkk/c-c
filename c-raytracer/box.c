/******************************************
* Tiffany Verkaik 
* ray tracer, "box.c"
******************************************/
#include "vector.h"
#include "ray.h"
#include <stdio.h>
#include <math.h>

int swap(double *t2, double *t1) {

   double *temp;

   temp = t2;
   t2 = t1;
   t1 = temp;

   return 1;
}

//box intersection test
int box_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t) { 

   int i, index, swapped;
   double t1, t2, t_near, t_far;
   double or[3], v[3], ll[3], ur[3];

   or[0] = ray.or.x;
   or[1] = ray.or.y;
   or[2] = ray.or.z;

   v[0] = ray.v.x;
   v[1] = ray.v.y;
   v[2] = ray.v.z;

   ll[0] = objs.type.box.ll.x;
   ll[1] = objs.type.box.ll.y;
   ll[2] = objs.type.box.ll.z;

   ur[0] = objs.type.box.ur.x;
   ur[1] = objs.type.box.ur.y;
   ur[2] = objs.type.box.ur.z;

  
   static POINT_T normal[6] = { {-1.0, 0.0, 0.0}, {1.0, 0.0, 0.0},
                                {0.0, -1.0, 0.0}, {0.0, 1.0, 0.0},
                                {0.0, 0.0, -1.0}, {0.0, 0.0, 1.0} };
   t_near = -500.0;
   t_far  = 500.0;
 
    for(i = 0; i < 3; i++) {
      if(or[i] == 0) { 
         if(or[i] < ll[i] || or[i] > ur[i]) return 0;
      }

      else {
         t1 = (ll[i] - or[i]) / v[i];
         t2 = (ur[i] - or[i]) / v[i];
         if(t1 > t2) swapped = swap(&t2, &t1);
         if(t1 > t_near) {
            t_near = t1;
            index = 2i;
            if(swapped == 1)
               index = 2i + 1;
         }
         if(t2 < t_far) t_far = t2;
         if(t_near > t_far) return 0;
         if(t_far < 0) return 0;
      }
      swapped = 0;
   }

   *t = t_near;
   *n = normal[index];
   int_pt ->x = or[0] + v[0] + (*t);  
   int_pt ->y = or[1] + v[1] + (*t);  
   int_pt ->z = or[3] + v[2] + (*t);  

   return 1;
}

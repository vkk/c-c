/******************************************
* Tiffany Verkaik 
* ray tracer, "vector.c"
******************************************/
#include "vector.h"
#include <stdio.h>
#include <math.h>

//function to return the length of the vector
double v_len(POINT_T v) {

   return sqrt((v.x*v.x) + (v.y*v.y) + (v.z*v.z));
}

//function to return addition of two vectors
POINT_T v_add(POINT_T v1, POINT_T v2) {

   POINT_T v3;
   v3.x = (v1.x + v2.x);
   v3.y = (v1.y + v2.y);
   v3.z = (v1.z + v2.z);
   return v3;
}

//function to return the subtraction of two vectors
POINT_T v_sub(POINT_T v1, POINT_T v2) {

   POINT_T v3;
   v3.x = (v1.x - v2.x);
   v3.y = (v1.y - v2.y);
   v3.z = (v1.z - v2.z);
   return v3; 
}

//function to return the product of two vectors
double v_dot(POINT_T v1, POINT_T v2) {

    return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
}

//function to scale a vector by a number
POINT_T v_scl(POINT_T v, double k) {

   POINT_T vk;
   vk.x = (k * v.x);
   vk.y = (k * v.y);
   vk.z = (k * v.z);
   return vk;
}

//function to return the 'normal' of a vector
POINT_T v_norm(POINT_T v) {

   POINT_T vn;
   double length;

   length = v_len(v);
   vn.x = (v.x / length);
   vn.y = (v.y / length);
   vn.z = (v.z / length);
   return vn;
}

//function to copy a vector to pointer result_v
void v_copy(POINT_T *result_v, POINT_T v) {

   result_v ->x = v.x;
   result_v ->y = v.y;
   result_v ->z = v.z;
}

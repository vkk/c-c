/******************************************
* Tiffany Verkaik 
* ray tracer, "ray.h"
******************************************/
#ifndef RAY_H
#define RAY_H

#define NUM_OBJS 4
#define NUM_LIGHTS 2
#define IMG_WD 640
#define IMG_HT 480

#include "vector.h"
#include <stdio.h>

//color type structure
typedef struct {

   double r;
   double g;
   double b;

} COLOR_T;

//sphere type structure
//'r' is radius; 'c' is centerpoint (x, y, z)
typedef struct {

   double r;
   POINT_T c;

} SPHERE_T;

//ray type structure
//'or' is origin (x, y, z); 'v' is direction (x, y, z)
typedef struct {

   POINT_T or;
   POINT_T v;

} RAY_T;

//plane type structure
//'d' is distance from origin; 'n' is normal (x, y, z)
typedef struct {

   double d;
   POINT_T n;

} PLANE_T;

typedef struct {

   POINT_T ll;
   POINT_T ur;

} BOX_T;

typedef struct LIGHT_TYPE {

   POINT_T c;
   COLOR_T color;
   struct LIGHT_TYPE *next;

} LIGHT_T;

//object type structure
//'*int_obj' is the intersection-test function pointer 
//'*get_c' is function pointer to fetch the object color
typedef struct OBJ_TYPE {

   union {
      SPHERE_T sphere;
      PLANE_T plane;
      BOX_T box;
   } type;
   
   int (*int_obj)(struct OBJ_TYPE obj, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t);
   COLOR_T (*get_c)(struct OBJ_TYPE obj, POINT_T int_pt);
   COLOR_T color;
   COLOR_T color2;
   double reflect;
   struct OBJ_TYPE  *next;

} OBJ_T;

//image type structure
typedef struct {

   int wd, ht, max_pix_val;
   COLOR_T **pixels;

} IMG_T;

//scene type structure
//'img_beg' is the location of 1st pixel (x, y, z)
typedef struct {

   IMG_T img;
   POINT_T img_begin;
   OBJ_T *objs;
   LIGHT_T *lights;

} SCENE_T;

#endif  

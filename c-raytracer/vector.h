/******************************************
* Tiffany Verkaik 
* ray tracer, "vector.h"
******************************************/
#ifndef VECTOR_H
#define VECTOR_H

#include <stdio.h>

//3D struct defined
typedef struct {

   double x;
   double y;
   double z;

} POINT_T;

//prototype statements  for 3D vectors
double v_len(POINT_T v);
double v_dot(POINT_T v1, POINT_T v2);
POINT_T v_add(POINT_T v1, POINT_T v2);
POINT_T v_sub(POINT_T v1, POINT_T v2);
POINT_T v_scl(POINT_T v, double k);
POINT_T v_norm(POINT_T v);
void v_copy(POINT_T *result_v, POINT_T v);

#endif

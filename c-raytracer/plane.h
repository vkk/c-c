/******************************************
* Tiffany Verkaik 
* ray tracer, "plane.h"
******************************************/
#ifndef PLANE_H
#define PLANE_H

#include <stdio.h>

int plane_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t);

#endif

/******************************************
* Tiffany Verkaik 
* ray tracer, "ray.c"
******************************************/
#include "vector.h"
#include "ray.h"
#include "light.h"
#include "sphere.h"
#include "plane.h"
#include "box.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void init(FILE *fPtr, SCENE_T *scene, IMG_T *img) {
   
   int i;
   char line[100];
   OBJ_T *obj;
   LIGHT_T *light;

            
   scene ->objs = NULL;
   scene ->lights = NULL;
   img ->max_pix_val = 255;

   while(fscanf(fPtr, "%s", line) != EOF) {
      //writing image width
      if(strcmp(line, "width") == 0) {
         fscanf(fPtr, "%d", &img ->wd);
      }

      //writing image height
      else if(strcmp(line, "height") == 0) {
         fscanf(fPtr, "%d", &img ->ht);
      }

      //writing sphere image, creating object link
      else if(strcmp(line, "sphere") == 0) {
         obj = (OBJ_T *)malloc(sizeof(OBJ_T));
         obj ->int_obj = sphere_int;
         obj ->get_c = set_color;
         fscanf(fPtr, "%lf %lf %lf %lf %lf %lf %lf %lf", 
            &obj ->type.sphere.c.x, &obj ->type.sphere.c.y, &obj ->type.sphere.c.z,
            &obj ->type.sphere.r, 
            &obj ->color.r, &obj ->color.g, &obj ->color.b, 
            &obj ->reflect);
         obj ->next = scene ->objs;
         scene ->objs = obj;
      }

      //writing checkerboard plane
      else if(strcmp(line, "plane") == 0) {
         obj = (OBJ_T *)malloc(sizeof(OBJ_T));
         obj ->int_obj = plane_int;
         obj ->get_c = checker;
         fscanf(fPtr, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", 
            &obj ->type.plane.n.x, &obj ->type.plane.n.y, &obj ->type.plane.n.z,
            &obj ->type.plane.d,
            &obj ->color.r, &obj ->color.g, &obj ->color.b, 
            &obj ->color2.r, &obj ->color2.g, &obj ->color2.b,
            &obj ->reflect);
         obj ->next = scene ->objs;
         scene ->objs = obj;
      }

      //writing box
      else if(strcmp(line, "box") == 0) {
         obj = (OBJ_T *)malloc(sizeof(OBJ_T));
         obj ->int_obj = box_int;
         obj ->get_c = set_color;
         fscanf(fPtr, "%lf %lf %lf %lf %lf %lf %lf %lf %lf %lf", 
            &obj ->type.box.ll.x, &obj ->type.box.ll.y, &obj ->type.box.ll.z,
            &obj ->type.box.ur.x, &obj ->type.box.ur.y, &obj ->type.box.ur.z,
            &obj ->color.r, &obj ->color.g, &obj ->color.b, 
            &obj ->reflect); 
         obj ->next = scene ->objs;
         scene ->objs = obj;
      }    

     //writing light, creating light link
     else if(strcmp(line, "light") == 0) {
         light = (LIGHT_T *)malloc(sizeof(LIGHT_T));
         fscanf(fPtr, "%lf %lf %lf %lf %lf %lf", 
            &light ->c.x, &light ->c.y, &light ->c.z,
            &light ->color.r, &light ->color.g, &light ->color.b);
         light ->next = scene ->lights;
         scene ->lights = light;
     }

   //writing image memory with double pointer
   scene ->img.pixels = (COLOR_T **)malloc(img ->ht * sizeof(COLOR_T *));
   for(i = 0; i < img ->ht; i++) {   
      scene ->img.pixels[i] = (COLOR_T *)malloc(img ->wd * sizeof(COLOR_T));
   }

   //location of first pixel
   scene ->img_begin.x = -0.67,
   scene ->img_begin.y = 0.5,
   scene ->img_begin.z = 1.0;
   } 
}

COLOR_T trace(SCENE_T scene, RAY_T ray) {

   COLOR_T reflect_c;
   COLOR_T obj_color = {0.3, 0.3, 0.5};
   OBJ_T *closest_obj = NULL;
   OBJ_T *obj;
   POINT_T int_pt, n, closest_int_pt, closest_n;
   double t, closest_t;

   closest_t = 500.0; 

   for(obj = scene.objs; obj != NULL; obj = obj ->next) {
      if(obj ->int_obj(*obj, ray, &int_pt, &n, &t)) { 
         if(t < closest_t) {
            closest_obj = obj;
            closest_int_pt = int_pt;
            closest_n = n;
            closest_t = t;
         }
      }
   }

   if(closest_obj != NULL) {
      obj_color = do_light(scene, closest_obj, ray, closest_int_pt, closest_n, closest_t);
      if(closest_obj ->reflect > 0.0) {
         reflect_c = trace(scene, compute_reflect(ray, closest_n));
         obj_color = c_add(c_scl(reflect_c, (closest_obj ->reflect)), 
                           c_scl(obj_color, (1 - closest_obj ->reflect))); 
      }   
   }
   return obj_color;
}

int main(int argc, char *argv[]) {

   if(argc != 4) {
      fprintf(stderr, "Usage is scene.txt out.ppm [1,anti-aliasing option OR 0,none]\n");
      exit (1);
   }

   FILE *in = fopen(argv[1], "r");
   if(in == NULL) {
      fprintf(stderr, "Input file not opened\n");
      exit (1);
   }

   FILE *out = fopen(argv[2], "w");
   if(out == NULL) {
      fprintf(stderr, "Output file not opened\n");
      exit (1);
   }

   SCENE_T scene;
   IMG_T img;
   
   init(in, &scene, &img);
   fclose(in);  
 
   fprintf(out,"P6\n%d %d\n255\n", img.wd, img.ht);
   
   int i, x, y, anti_alias;
   double res;
   RAY_T ray;

   anti_alias = strcmp(argv[3], "0");
   img.pixels = (COLOR_T**)malloc(img.ht * sizeof(COLOR_T*));
  
   for(i = 0; i < img.ht; i++) 
      img.pixels[i] = (COLOR_T*)malloc(img.wd * sizeof(COLOR_T));   

   if(img.ht < img.wd) {
      res = 1.0 / img.ht; 
      scene.img_begin.x = (-img.wd / (float)img.ht) / 2;
      scene.img_begin.y = 0.5;
   }
   if(img.wd < img.ht) {
      res = 1.0 / img.wd;
      scene.img_begin.x = (-img.ht / (float)img.wd) / 2;
      scene.img_begin.y = 0.5;
   }
   else if(img.ht == img.wd) {
      res = 1.0 / img.ht;
      scene.img_begin.x = -0.5;
      scene.img_begin.y = 0.5;
   }

   for(y = 0; y < img.ht; y++) {
      for(x = 0; x < img.wd; x++) {
         ray.or.x = 0.0; 
         ray.or.y = 0.0; 
         ray.or.z = 0.0; 
         ray.v.x = scene.img_begin.x + (res * x);
         ray.v.y = scene.img_begin.y - (res * y);
         ray.v.z = scene.img_begin.z;
         ray.v  = v_norm(ray.v);

         COLOR_T img_color = trace(scene, ray);
                
         img.pixels[y][x].r = img_color.r;
         img.pixels[y][x].g = img_color.g;
         img.pixels[y][x].b = img_color.b; 
      }
   }

   int count = 0;
   COLOR_T final_pixel = {0, 0, 0};
 
   if(anti_alias) {
      for(y = 0; y < img.ht; y++) {
         for(x = 0; x < img.wd; x++) {
            final_pixel.r = img.pixels[y][x].r;
            final_pixel.g = img.pixels[y][x].g;
            final_pixel.b = img.pixels[y][x].b;

            final_pixel = c_scl(final_pixel, 9);
            count = 9;
 
            if(x != 0) { 
               final_pixel = c_add(final_pixel, c_scl(img.pixels[y][x - 1], 3));
               count += 3;
               if(x != img.wd - 1) {
                  final_pixel = c_add(final_pixel, c_scl(img.pixels[y][x + 1], 3));
                  count += 3;
               }
               if(y != 0) {
                  final_pixel = c_add(final_pixel, (img.pixels[y - 1][x - 1]));
                  count++;
                  if(y != img.ht - 1) {
                     final_pixel = c_add(final_pixel, (img.pixels[y + 1][x - 1]));
                     count++;
                  }
               }
            }

            if(y != 0) {
               final_pixel = c_add(final_pixel, c_scl(img.pixels[y - 1][x], 3));
               count += 3;
               if(y != img.ht - 1) { 
                  final_pixel = c_add(final_pixel, c_scl(img.pixels[y + 1][x], 3));
                  count += 3;
               }
               if(x != 0) {
                  final_pixel = c_add(final_pixel, (img.pixels[y - 1][x + 1]));
                  count++;
                  if(x != img.wd - 1 && y != img.ht - 1) {
                     final_pixel = c_add(final_pixel, (img.pixels[y + 1][x + 1]));
                     count++;
                  }
               }
            }

            final_pixel = c_scl(final_pixel, 1.0 / count);
            if(final_pixel.r > 1.0) final_pixel.r = 1.0;
            if(final_pixel.g > 1.0) final_pixel.g = 1.0;
            if(final_pixel.b > 1.0) final_pixel.b = 1.0;
            final_pixel = c_scl(final_pixel, 255);
            fprintf(out,"%c%c%c", (unsigned char)final_pixel.r, 
                                  (unsigned char)final_pixel.g, 
                                  (unsigned char)final_pixel.b);
         }
      }
   }

   else {
      for(y = 0; y < img.ht; y++) {
         for(x = 0; x < img.wd; x++) {
            if(img.pixels[y][x].r > 1.0) img.pixels[y][x].r = 1.0;
            if(img.pixels[y][x].g > 1.0) img.pixels[y][x].g = 1.0;
            if(img.pixels[y][x].b > 1.0) img.pixels[y][x].b = 1.0;
            img.pixels[y][x] = c_scl(img.pixels[y][x], 255);
            fprintf(out,"%c%c%c", (unsigned char)img.pixels[y][x].r,
                                  (unsigned char)img.pixels[y][x].g,
                                  (unsigned char)img.pixels[y][x].b);
         }
      }
   }
   
   return 0;
   fclose(out);
}

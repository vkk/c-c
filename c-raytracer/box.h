/******************************************
* Tiffany Verkaik 
* ray tracer, "box.h"
******************************************/
#ifndef BOX_H
#define BOX_H

#include <stdio.h>

int swap(double *t2, double *t1);
int box_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t);

#endif

/******************************************
* Tiffany Verkaik 
* ray tracer, "plane.c"
******************************************/
#include "vector.h"
#include "ray.h"
#include <stdio.h>
#include <math.h>

//plane intersection test
int plane_int(OBJ_T objs, RAY_T ray, POINT_T *int_pt, POINT_T *n, double *t) {

   double num = -(v_dot(objs.type.plane.n, ray.or) + objs.type.plane.d); 
   double den = v_dot(objs.type.plane.n, ray.v);

   if(den == 0) return 0;
   else *t = num / den;

   if(*t <= 0.0) return 0;
   if(*t > 0.0) {
      int_pt ->x = ray.or.x + ray.v.x * (*t);
      int_pt ->y = ray.or.y + ray.v.y * (*t);
      int_pt ->z = ray.or.z + ray.v.z * (*t);
     
      n ->x = objs.type.plane.n.x;
      n ->y = objs.type.plane.n.y;
      n ->z = objs.type.plane.n.z;
   }
   return 1;
}
